<?php //netteCache[01]000588a:2:{s:4:"time";s:21:"0.49878100 1502841806";s:9:"callbacks";a:4:{i:0;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:9:"checkFile";}i:1;s:101:"/home/iliwyliw/ispylagos.com/wp-content/themes/cityguide/ait-theme/elements/countdown/countdown.latte";i:2;i:1500575282;}i:1;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:20:"NFramework::REVISION";i:2;s:22:"released on 2014-08-28";}i:2;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:15:"WPLATTE_VERSION";i:2;s:5:"2.9.2";}i:3;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:17:"AIT_THEME_VERSION";i:2;s:4:"3.10";}}}?><?php

// source file: /home/iliwyliw/ispylagos.com/wp-content/themes/cityguide/ait-theme/elements/countdown/countdown.latte

?><?php
// prolog NCoreMacros
list($_l, $_g) = NCoreMacros::initRuntime($template, 'ai7ebhk7fd')
;
// prolog NUIMacros

// snippets support
if (!empty($_control->snippetMode)) {
	return NUIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
NCoreMacros::includeTemplate($element->common('header'), $template->getParameters(), $_l->templates['ai7ebhk7fd'])->render() ?>

<div id="<?php echo NTemplateHelpers::escapeHtml($htmlId, ENT_COMPAT) ?>" class="<?php echo NTemplateHelpers::escapeHtml($htmlClass, ENT_COMPAT) ?>">

	<div class="clock-container">
		<div class="clock-item days-container">
			<div class="clock-icon">
				<canvas width="<?php echo $el->option('countdownSize') ?>" height="<?php echo $el->option('countdownSize') ?>">
					<?php echo NTemplateHelpers::escapeHtml(__('Your browser does not support the HTML5 canvas tag.', 'wplatte'), ENT_NOQUOTES) ?>

				</canvas>
			</div>
			<div class="clock-data">
				<p class="clock-value days-value">0</p>
				<p class="clock-text days-text"><?php echo NTemplateHelpers::escapeHtml(__('DAYS', 'wplatte'), ENT_NOQUOTES) ?></p>
			</div>
		</div><!--
	 --><div class="clock-item hours-container">
	 		<div class="clock-icon">
				<canvas width="<?php echo $el->option('countdownSize') ?>" height="<?php echo $el->option('countdownSize') ?>">
					<?php echo NTemplateHelpers::escapeHtml(__('Your browser does not support the HTML5 canvas tag.', 'wplatte'), ENT_NOQUOTES) ?>

				</canvas>
			</div>
			<div class="clock-data">
				<p class="clock-value hours-value">0</p>
				<p class="clock-text hours-text"><?php echo NTemplateHelpers::escapeHtml(__('HRS', 'wplatte'), ENT_NOQUOTES) ?></p>
			</div>
	 	</div><!--
	 --><div class="clock-item minutes-container">
	 		<div class="clock-icon">
				<canvas width="<?php echo $el->option('countdownSize') ?>" height="<?php echo $el->option('countdownSize') ?>">
					<?php echo NTemplateHelpers::escapeHtml(__('Your browser does not support the HTML5 canvas tag.', 'wplatte'), ENT_NOQUOTES) ?>

				</canvas>
			</div>
			<div class="clock-data">
				<p class="clock-value minutes-value">0</p>
				<p class="clock-text minutes-text"><?php echo NTemplateHelpers::escapeHtml(__('MINS', 'wplatte'), ENT_NOQUOTES) ?></p>
			</div>
	 	</div><!--
	 --><div class="clock-item seconds-container">
	 		<div class="clock-icon">
				<canvas width="<?php echo $el->option('countdownSize') ?>" height="<?php echo $el->option('countdownSize') ?>">
					<?php echo NTemplateHelpers::escapeHtml(__('Your browser does not support the HTML5 canvas tag.', 'wplatte'), ENT_NOQUOTES) ?>

				</canvas>
			</div>
			<div class="clock-data">
				<p class="clock-value seconds-value">0</p>
				<p class="clock-text seconds-text"><?php echo NTemplateHelpers::escapeHtml(__('SECS', 'wplatte'), ENT_NOQUOTES) ?></p>
			</div>
		</div>
	</div>
	<div class="clock-done entry-content"><?php echo $el->option('finishedText') ?></div>

</div>

<?php NCoreMacros::includeTemplate(WpLatteMacros::getTemplatePart("ait-theme/elements/countdown/javascript", ""), array() + get_defined_vars(), $_l->templates['ai7ebhk7fd'])->render() ;