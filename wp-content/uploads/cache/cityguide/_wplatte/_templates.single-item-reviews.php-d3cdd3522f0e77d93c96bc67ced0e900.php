<?php //netteCache[01]000584a:2:{s:4:"time";s:21:"0.58316200 1503427047";s:9:"callbacks";a:4:{i:0;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:9:"checkFile";}i:1;s:98:"/home/iliwyliw/ispylagos.com/wp-content/plugins/ait-item-reviews/templates/single-item-reviews.php";i:2;i:1502392315;}i:1;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:20:"NFramework::REVISION";i:2;s:22:"released on 2014-08-28";}i:2;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:15:"WPLATTE_VERSION";i:2;s:5:"2.9.2";}i:3;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:17:"AIT_THEME_VERSION";i:2;s:4:"3.10";}}}?><?php

// source file: /home/iliwyliw/ispylagos.com/wp-content/plugins/ait-item-reviews/templates/single-item-reviews.php

?><?php
// prolog NCoreMacros
list($_l, $_g) = NCoreMacros::initRuntime($template, 'b7qekcg8ca')
;
// prolog NUIMacros

// snippets support
if (!empty($_control->snippetMode)) {
	return NUIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
$ratingsDisplayedClass = AitItemReviews::hasReviewQuestions($post->id) ? 'ratings-shown' : 'ratings-hidden' ?>

<?php $ratingFormShownClass = 'rating-form-shown' ;if ($options->theme->itemReviews->onlyRegistered) { if (!is_user_logged_in()) { $ratingFormShownClass = 'rating-form-hidden' ;} } ?>

<div class="reviews-container <?php echo NTemplateHelpers::escapeHtml($ratingsDisplayedClass, ENT_COMPAT) ?>
 <?php echo NTemplateHelpers::escapeHtml($ratingFormShownClass, ENT_COMPAT) ?>" id="review">
<?php if ($options->theme->itemReviews->onlyRegistered) { if (is_user_logged_in()) { ?>
			<h2><?php _e('Leave a Review', 'ait-item-reviews') ?></h2>
<?php NCoreMacros::includeTemplate(WpLatteMacros::getTemplatePart("portal/parts/single-item-reviews-form", ""), array() + get_defined_vars(), $_l->templates['b7qekcg8ca'])->render() ;} else { ?>
			<h2><?php _e('Leave a Review', 'ait') ?></h2>
			<div class="current-rating-container review-stars-container">
				<h3><?php echo $options->theme->itemReviews->onlyRegisteredMessage ?></h3>
			</div>
<?php } } else { ?>
		<h2><?php _e('Leave a Review', 'ait-item-reviews') ?></h2>
<?php NCoreMacros::includeTemplate(WpLatteMacros::getTemplatePart("portal/parts/single-item-reviews-form", ""), array() + get_defined_vars(), $_l->templates['b7qekcg8ca'])->render() ;} ?>

<?php $reviews_query = AitItemReviews::getCurrentItemReviews($post->id) ;if (count($reviews_query->posts) > 0) { ?>
	<div class="content">
<?php foreach ($iterator = new WpLatteLoopIterator($reviews_query) as $review): ?>

<?php $rating_overall = get_post_meta($review->id, 'rating_mean', true) ;$rating_data = (array)json_decode(get_post_meta($review->id, 'ratings', true)) ?>

<?php $ratingsDisplayedClass = AitItemReviews::willRatingsDisplay($rating_data) ? 'ratings-shown' : 'ratings-hidden' ?>

<?php $dateFormat = get_option('date_format') ?>

		<div class="review-container <?php echo NTemplateHelpers::escapeHtml($ratingsDisplayedClass, ENT_COMPAT) ?>">
			<div class="review-info">
				<span class="review-name"><?php echo NTemplateHelpers::escapeHtml($review->title, ENT_NOQUOTES) ?></span>
				<span class="review-time"><span><?php echo NTemplateHelpers::escapeHtml($template->dateI18n($review->rawDate, $dateFormat), ENT_NOQUOTES) ?>
</span>&nbsp;<span><?php echo NTemplateHelpers::escapeHtml($review->time(), ENT_NOQUOTES) ?></span></span>
<?php if (is_array($rating_data) && count($rating_data) > 0) { ?>
				<div class="review-stars">
					<span class="review-rating-overall" data-score="<?php echo NTemplateHelpers::escapeHtml($rating_overall, ENT_COMPAT) ?>"></span>
					<div class="review-ratings">
<?php $iterations = 0; foreach ($rating_data as $index => $rating) { if ($rating->question) { ?>
							<div class="review-rating">
								<span class="review-rating-question">
									<?php echo NTemplateHelpers::escapeHtml($rating->question, ENT_NOQUOTES) ?>

								</span>
								<span class="review-rating-stars" data-score="<?php echo NTemplateHelpers::escapeHtml($rating->value, ENT_COMPAT) ?>"></span>
							</div>
<?php } $iterations++; } ?>
					</div>
				</div>
<?php } ?>
			</div>
			<div class="content">
				<?php echo $review->content ?>

			</div>
						<script type="application/ld+json">
			{
				"@context": "http://schema.org/",
				"@type": "Review",
				"itemReviewed": {
					"@type": "Thing",
					"name": "<?php echo $post->title ?>"
				},
				"reviewRating": {
					"@type": "Rating",
					"ratingValue": "<?php echo $rating_overall ?>"
				},
				"author": {
					"@type": "Person",
					"name": "<?php echo $review->title ?>"
				},
				"reviewBody": "<?php echo strip_tags($review->content) ?>"
			}
			</script>
					</div>
<?php endforeach; wp_reset_postdata() ?>


		<script type="text/javascript">
			jQuery(document).ready(function() {

				/* Review Tooltip Off-screen Check */

				jQuery('#review .review-container:nth-last-of-type(-n+3) .review-stars').mouseenter(function() {
					reviewOffscreen(jQuery(this));
				});

				function reviewOffscreen(rating) {
					var reviewContainer = rating.find('.review-ratings');
					if (!reviewContainer.hasClass('off-screen')) {
						var	bottomOffset = jQuery(document).height() - rating.offset().top - reviewContainer.outerHeight() - 30;
						if (bottomOffset < 0) reviewContainer.addClass('off-screen');
					}
				}
			});
		</script>

	</div>
<?php } ?>
</div>
