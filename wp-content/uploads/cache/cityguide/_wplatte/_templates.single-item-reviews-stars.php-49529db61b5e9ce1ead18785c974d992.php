<?php //netteCache[01]000584a:2:{s:4:"time";s:21:"0.38627900 1505107919";s:9:"callbacks";a:4:{i:0;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:9:"checkFile";}i:1;s:98:"/home/ubuntu/workspace/wp-content/plugins/ait-item-reviews/templates/single-item-reviews-stars.php";i:2;i:1504422873;}i:1;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:20:"NFramework::REVISION";i:2;s:22:"released on 2014-08-28";}i:2;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:15:"WPLATTE_VERSION";i:2;s:5:"2.9.2";}i:3;a:3:{i:0;a:2:{i:0;s:6:"NCache";i:1;s:10:"checkConst";}i:1;s:17:"AIT_THEME_VERSION";i:2;s:4:"3.10";}}}?><?php

// source file: /home/ubuntu/workspace/wp-content/plugins/ait-item-reviews/templates/single-item-reviews-stars.php

?><?php
// prolog NCoreMacros
list($_l, $_g) = NCoreMacros::initRuntime($template, '6y8j5wpwum')
;
// prolog NUIMacros

// snippets support
if (!empty($_control->snippetMode)) {
	return NUIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
$rating_count = AitItemReviews::getRatingCount($post->id) ;$rating_mean = floatval(get_post_meta($post->id, 'rating_mean', true)) ?>

<?php $showCount = isset($showCount) ? $showCount : false ?>
<div class="review-stars-container">
<?php if ($rating_count > 0) { ?>
		<div class="content" itemscope itemtype="http://schema.org/AggregateRating">
						<span style="display: none" itemprop="itemReviewed"><?php echo $post->title ?></span>
			<span style="display: none" itemprop="ratingValue"><?php echo NTemplateHelpers::escapeHtml($rating_mean, ENT_NOQUOTES) ?></span>
			<span style="display: none" itemprop="ratingCount"><?php echo NTemplateHelpers::escapeHtml($rating_count, ENT_NOQUOTES) ?></span>
						<span class="review-stars" data-score="<?php echo NTemplateHelpers::escapeHtml($rating_mean, ENT_COMPAT) ?>"></span>
			<?php if ($showCount) { ?><span class="review-count">(<?php echo NTemplateHelpers::escapeHtml($rating_count, ENT_NOQUOTES) ?>
)</span><?php } ?>

			<a href="<?php echo NTemplateHelpers::escapeHtml($post->permalink, ENT_COMPAT) ?>
#review"><?php _e('Submit your rating', 'ait-item-reviews') ?></a>
		</div>
<?php } else { ?>
		<div class="content">
			<a href="<?php echo NTemplateHelpers::escapeHtml($post->permalink, ENT_COMPAT) ?>
#review"><?php _e('Submit your rating', 'ait-item-reviews') ?></a>
		</div>
<?php } ?>
</div>