<?php if( !empty( $role ) ) {
    $tr = $k.'_'.$role.'_tr';
}
else {
    $tr = $k.'_tr';
    $role = "";
}
$userpro_data = get_option('userpro');
if(!isset($userpro_data['allowed_roles'])){
    $userpro_data['allowed_roles'] = array('subscriber');
}
$role_array = $userpro_data['allowed_roles'];
if(!in_array($k, $role_array)){
?>
<tr id = "<?php echo $tr; ?>" valign="top" data-role="<?php echo $role ?>">
	<td class= "fields_name" id = "<?php echo $k; ?>"> 
		<label for="<?php echo $k; ?>"><?php if(!empty($v['displayName']))echo $v['displayName']; ?></label> 	
		<input class="fields_name_hidden" id= "text_<?php echo $k;?>" type = "hidden" name = "field_name_hidden" value="<?php echo $k; ?>" />
	</td> 
	<td class = "fields_percentage" id = "<?php echo $k.'_'.$v['percentage']; ?>">    
		<label for="<?php echo $k.'_'.$v['percentage']; ?>"><?php echo $v['percentage']; ?></label> 

		<input style="display:none;" class="fields_input_percentage" id= "text_<?php echo $k;?>" type = "text" name = "edit" value="<?php echo $v['percentage']; ?>" />
	</td>
	<td class = "fields_action">
		<div class="fields_edit_btn" id= "edit_<?php echo $k;?>"></div>
		<div class="fields_save_btn" style="display:none;" id= "save_<?php echo $k;?>"></div>
		<div class="fields_delete_btn" style="display:none;" id= "delete_<?php echo $k;?>"></div>
	</td>
</tr>	
<?php } ?>

