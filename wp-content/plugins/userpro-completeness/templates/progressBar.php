<?php 
    $userpro_completeness_api = new userpro_completeness_api(); 
    $userpro_data = get_option('userpro');
    if(!isset($userpro_data['allowed_roles'])){
        $userpro_data['allowed_roles'] = array('subscriber');
    }
    $role_array = $userpro_data['allowed_roles'];
    if(isset($args['user_id'])){
            $current_user_percentage = $userpro_completeness_api->get_completeness_usermeta_info($args['user_id']);
    }
    else{
            $current_user_percentage = 0;
    }
    $user_id = isset($args['user_id']) ? $args['user_id'] : get_current_user_id(); 
    $userpro_completeness_save_field = get_option('userpro_completeness_save_field');
    $data = "";
    $up_user = wp_get_current_user();
    if (isset($up_user->roles[0]) && !empty($userpro_completeness_save_field[$up_user->roles[0]]) && in_array($up_user->roles[0], $role_array)) {
        foreach($userpro_completeness_save_field[$up_user->roles[0]] as $k => $v){
            $data.='data-'.$k.'='.$v['percentage'].' ';
        }
    }
    else {
        foreach($userpro_completeness_save_field as $k => $v){ 
            if(!empty($v['percentage'])){
                $data.='data-'.$k.'='.$v['percentage'].' ';
            }
        }
    }
?>
<div id="data_field" <?php echo $data; ?> ></div>

<?php

      $progressbar_color=userpro_completeness_get_option('progress_bar');
      if(!isset($progressbar_color))$progressbar_color="default";
?>

<div id="progressBar" class="jquery-ui-like"><div class="<?php echo $progressbar_color;?>"></div><span></span></div>

<script>
	completeness_progressBar('<?php echo $current_user_percentage;?>' , jQuery('#progressBar'));
</script>
