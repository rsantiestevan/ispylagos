<?php
class userpro_completeness_api{	
	function __construct() {
		
	}
	
	function calculate_Percentage($role = ""){
		$total_percentage = 100;
		$field_percentage = 0;
		$userpro_completeness_save_field = get_option('userpro_completeness_save_field'); 
		//$all_roles = array('admin','editor','author','contributor','subscriber','customer','shopmanager');
                $userpro_data = get_option('userpro');
                if(!isset($userpro_data['allowed_roles'])){
                    $userpro_data['allowed_roles'] = array('subscriber');
                }
                $all_roles = $userpro_data['allowed_roles'];
                if(in_array($role, $all_roles)){
                        if(!empty($userpro_completeness_save_field[$role])){
                                foreach($userpro_completeness_save_field[$role] as $k=>$v) {
                                        if($field_percentage <= $total_percentage && !empty($v['percentage'])){
                                                $field_percentage += $v['percentage'];
                                        }
                                }
                        }
		}
                else{
                        if(!empty($userpro_completeness_save_field)){
                                foreach($userpro_completeness_save_field as $k=>$v) {
                                        if(!in_array($k,$all_roles)){
                                                if($field_percentage <= $total_percentage && !empty($v['percentage'])){
                                                        $field_percentage += $v['percentage'];
                                                }
                                        }
                                }
                        }
		}
		return $field_percentage;
	}


	function get_completeness_usermeta_info($user_id = null){
		$user_id = isset($user_id) ? $user_id : get_current_user_id(); 
                $up_user = wp_get_current_user();
                $userpro_data = get_option('userpro');
                if(!isset($userpro_data['allowed_roles'])){
                    $userpro_data['allowed_roles'] = array('subscriber');
                }
                $userpro_roles = $userpro_data['allowed_roles'];
		$userpro_completeness_save_field = get_option('userpro_completeness_save_field');
		$current_user_cpercentage = 0;	
                if (isset($up_user->roles[0]) && !empty($userpro_completeness_save_field[$up_user->roles[0]])  && in_array($up_user->roles[0], $userpro_roles)) {
                    foreach($userpro_completeness_save_field[$up_user->roles[0]] as $k => $v){
				$user_meta_val = get_user_meta($user_id,$k,true);
				if( isset($user_meta_val) && !empty($user_meta_val))
				{
					$current_user_cpercentage += $v['percentage'];
				}
			}
                }
		elseif(!empty($userpro_completeness_save_field)){
			foreach($userpro_completeness_save_field as $k => $v){
				$user_meta_val = get_user_meta($user_id,$k,true);
				if( isset($user_meta_val) && !empty($user_meta_val))
				{
					$current_user_cpercentage += $v['percentage'];
				}
			}
		}
		return $current_user_cpercentage;
	}
}

$userpro_completeness_api = new userpro_completeness_api();
?>
