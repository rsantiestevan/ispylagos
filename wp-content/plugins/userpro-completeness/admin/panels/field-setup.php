<?php

	$total_percentage = 100;
	$userpro_completeness_api = new userpro_completeness_api();
	$total_field_percentage = $userpro_completeness_api->calculate_Percentage();
	$remaining_progress = $total_percentage - $total_field_percentage;
        $userpro_data = get_option('userpro');
        if(!isset($userpro_data['allowed_roles'])){
            $userpro_data['allowed_roles'] = array('subscriber');
        }
        $userpro_roles = $userpro_data['allowed_roles'];
        $userpro_threshold = userpro_completeness_get_option('userpro_completeness_threshold_percentage');
        if(empty($userpro_threshold)){
                $userpro_threshold_cal= 0;
            }
        else {
            if(is_numeric($userpro_threshold)){
               $userpro_threshold_cal = $userpro_threshold - $total_field_percentage ;
            }
            else{
                $userpro_threshold_cal = $total_field_percentage ;
            }
        }
?>
<h3><?php _e('Default Fields Setup','userpro-completeness'); ?></h3>
<table class="form-table">
<tr valign="top">
	<?php //$remaining_progress = 100 ?>
	<td>
	<div><?php _e('Remaining progress:','userpro-completeness'); ?> <strong><span class="userpro-remaining-progress userpro-remaining"><?php echo $remaining_progress; ?></span>%</strong></div>
	
        </td>
        
        <td>
            <?php if(isset($userpro_threshold_cal)) { 
                if($userpro_threshold_cal < 0){
                    $userpro_threshold_cal = 0 ;
                }
                ?>
            <div><?php _e('Remaining Threshold:','userpro-completeness'); ?> <strong><span class="userpro-remaining-threshold userpro-threshold"><?php echo $userpro_threshold_cal; ?></span>%</strong></div>
            <?php } ?>
            </td>
</tr>
<?php		$userpro_completeness_saved_fields=array();
		$userpro_completeness_saved_fields = get_option('userpro_completeness_save_field'); 
		if(!empty($userpro_completeness_saved_fields))		
		foreach($userpro_completeness_saved_fields as $k => $v){
                    if(!in_array($k, $userpro_roles)) {
                        include(UPC_PLUGIN_DIR.'/templates/percentageList.php'); 
                    }

	}?>



<tr id="add-field-tr" valign="top">
	<td>
	<input type="button" class="button" style="width:100px !important;" name="userpro-add-field" value="Add Field" id="userpro-add-field" />	
	</td>
</tr>
<tr valign="top" class="userpro-completeness-add">
	

		<th scope="row"><label for="select_fields"><?php _e('Select Field','userpro-completeness'); ?></label></th>
		<td>
			<select name="select_fields" id="select_fields" class="chosen-select" style="width:200px" data-placeholder="<?php _e('Select Fields','userpro-completeness'); ?>">
				<?php
				$flag = true;
				$field_names = get_option("userpro_fields_groups");
				foreach($field_names['edit']['default'] as $k=>$v) {
					if (array_key_exists('label', $v)) {
						$fields_having_percentage=array();
						$fields_having_percentage = get_option('userpro_completeness_save_field'); 
						
						if(!empty($fields_having_percentage))
						$fields_Keys = array_keys($fields_having_percentage,true);  
						else
						$fields_Keys = array();
							
								if(!empty($fields_Keys) && in_array($k,$fields_Keys)){  ?>
									<option value="<?php echo $k; ?>" ><?php echo $v['label']; ?></option>		

				<?php }
				      else{ 
				?>
									<option value="<?php echo $k; ?>" <?php echo $flag ? "selected" : ""; ?>><?php echo $v['label']; ?></option>
				<?php  
				$flag= false;
					} 
					}
				} ?>
			</select>
			<span class="description"><?php _e('Select edit profile form field to be included in profile completeness.','userpro'); ?></span>
		</td>
	</tr>
	<tr valign="top" class="userpro-completeness-add">
	<th scope="row"><label for="userpro-field-percentage"><?php _e('Field\'s Percentage','userpro-completeness'); ?></label></th>
	<td>
            
			<input type="text" style="width:300px !important;" name="userpro-field-percentage" id="userpro-field-percentage" value="<?php echo (userpro_completeness_get_option('userpro-field-percentage')) ? userpro_completeness_get_option('userpro-field-percentage') : ''; ?>" class="regular-text" />
			<span class="description"><?php _e('Enter Profile completion Threshold percentage.','userpro-completeness'); ?></span>
		</td>	
</tr>
<tr valign="top" class="userpro-completeness-add">
	<td>
	<input type="button" class="button" style="width:100px !important;" name="userpro-field-save" value="Save" id="userpro-field-save" />	
	</td>
</tr>
</table>
<?php 
 foreach ($userpro_roles as $userpro_role){
 ?>
<h3><?php _e(ucfirst($userpro_role).' Fields Setup','userpro-completeness'); ?></h3>
<?php
	$total_percentage = 100;
	$total_field_percentage_admin = $userpro_completeness_api->calculate_Percentage($userpro_role);
	$remaining_progress_admin = $total_percentage - $total_field_percentage_admin;
        $userpro_threshold = userpro_completeness_get_option('userpro_completeness_threshold_percentage');
        if(empty($userpro_threshold)){
            $userpro_threshold_cal= 0;
        }
        else {
            if(is_numeric($userpro_threshold)){
                $userpro_threshold_cal = $userpro_threshold - $total_field_percentage_admin ;
            }
            else{
                $userpro_threshold_cal = $total_field_percentage ;
            }
        }
?>
<table class="form-table">
    <tr valign="top">
            <?php //$remaining_progress = 100 ?>

            <td>
            <div><?php _e('Remaining progress:','userpro-completeness'); ?> <strong><span class="userpro-remaining-progress-<?php echo $userpro_role?> userpro-remaining"><?php echo $remaining_progress_admin; ?></span>%</strong></div>

            </td>

            <td>
            <?php if(isset($userpro_threshold_cal)) { 
                if($userpro_threshold_cal < 0){
                    $userpro_threshold_cal = 0 ;
                }
                ?>
            <div><?php _e('Remaining Threshold:','userpro-completeness'); ?> <strong><span class="userpro-remaining-threshold-<?php echo $userpro_role?> userpro-threshold"><?php echo $userpro_threshold_cal; ?></span>%</strong></div>
            <?php } ?>
            </td>

    </tr>
    <?php       
                $role = $userpro_role;
                $userpro_completeness_saved_fields=array();
		$userpro_completeness_saved_fields = get_option('userpro_completeness_save_field'); 
		if(!empty($userpro_completeness_saved_fields[$userpro_role]))		
		foreach($userpro_completeness_saved_fields[$userpro_role] as $k => $v){
		 include(UPC_PLUGIN_DIR.'/templates/percentageList.php'); 

	}
        ?>



<tr id="add-field-tr-<?php echo $userpro_role; ?>" valign="top">
	<td>
	<input type="button" class="button userpro-add-field" style="width:100px !important;" name="userpro-add-field-<?php echo $userpro_role; ?>" value="Add Field" id="userpro-add-field-<?php echo $userpro_role; ?>" data-role="<?php echo $userpro_role; ?>" />	
	</td>
</tr>
<tr valign="top" class="userpro-completeness-add-<?php echo $userpro_role; ?>">
	

		<th scope="row"><label for="select_fields_<?php echo $userpro_role; ?>"><?php _e('Select Field','userpro-completeness'); ?></label></th>
		<td>
			<select name="select_fields_<?php echo $userpro_role; ?>" id="select_fields_<?php echo $userpro_role; ?>" class="chosen-select" style="width:200px" data-placeholder="<?php _e('Select Fields','userpro-completeness'); ?>">
				<?php
				$flag = true;
				$field_names_admin = get_option("userpro_fields_groups");
				foreach($field_names_admin['edit']['default'] as $k=>$v) {
					if (array_key_exists('label', $v)) {
						$fields_having_percentage_admin=array();
						$fields_having_percentage_admin = get_option('userpro_completeness_save_field'); 
						if(!empty($fields_having_percentage_admin))
						$fields_Keys_admin = array_keys($fields_having_percentage_admin,true);  
						else
						$fields_Keys_admin = array();
							
								if(!empty($fields_Keys_admin) && in_array($k,$fields_Keys_admin)){  ?>
									<option value="<?php echo $k."_".$userpro_role; ?>" ><?php echo $v['label']; ?></option>		

				<?php }
				      else{ 
				?>
									<option value="<?php echo $k."_".$userpro_role; ?>" <?php echo $flag ? "selected" : ""; ?>><?php echo $v['label']; ?></option>
				<?php  
				$flag= false;
					} 
					}
				} ?>
			</select>
			<span class="description"><?php _e('Select edit profile form field to be included in profile completeness.','userpro'); ?></span>
		</td>
	</tr>
	<tr valign="top" class="userpro-completeness-add-<?php echo $userpro_role ?>">
	<th scope="row"><label for="userpro-field-percentage-<?php echo $userpro_role ?>"><?php _e('Field\'s Percentage','userpro-completeness'); ?></label></th>
	<td>
			<input type="text" style="width:300px !important;" name="userpro-field-percentage-<?php echo $userpro_role ?>" id="userpro-field-percentage-<?php echo $userpro_role ?>" value="<?php echo (userpro_completeness_get_option('userpro-field-percentage')) ? userpro_completeness_get_option('userpro-field-percentage') : ''; ?>" class="regular-text" />
			<span class="description"><?php _e('Enter Profile completion Threshold percentage.','userpro-completeness'); ?></span>
		</td>	
</tr>
<tr valign="top" class="userpro-completeness-add-<?php echo $userpro_role ?>">
    <td>
        <input type="button" class="button userpro-field-save" style="width:100px !important;" name="userpro-field-save" value="Save" id="userpro-field-save-<?php echo $userpro_role ?>" data-role="<?php echo $userpro_role ?>" />	
    </td>
</tr>
</table>
<?php 
 } 
 ?>
