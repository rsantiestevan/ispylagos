<?php 
if (is_user_logged_in() || userpro_media_get_option('media_display')=='n'){// if user is logged in OR Display Media to Logged In users only is set to "NO"
?>
	<div class="updb-widget-style">
		<div class="updb-basic-info"><?php _e( 'Video Gallery', 'userpro-mediamanager' );?></div>
		<div class="updb-view-profile-details"><br>
		<div class="updb-view-media-video">
		<?php 
			$arguments='';
			$media_args = shortcode_atts(array(
	        		'media' => 'view',
					'user_id'=>''
		    	),$arguments);
		
			if(empty($user_id))
			{
				$user_id = get_current_user_id();
			}
			
			$i = rand(1, 1000);
			$default_args =  array(
				"template"				=> "view",
				"media_display" 		=> "n",
				"user_id"				=> $user_id,
				"unique_id"				=> $i,
				"media"					=> $media_args['media'],
				"disable_photos_tab" 	=> 1,
				"disable_music_tab"		=> 1,
				"disable_instagram_tab"	=> 1,
				"disable_media_tab"		=> 1
			);
				
			echo userpro_media_manager($default_args);
		?>
		</div>
		
		</div>
	</div>
<?php }