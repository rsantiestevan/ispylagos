<form method="post" action="">
	<h3><?php _e('General Settings','userpro-media'); ?></h3>
	<table class="form-table">

	<tr valign="top">
		<th scope="row"><label for="upmg_enable_gallery"><?php _e('Enable Media Gallery','userpro-media'); ?></label></th>
		<td>
			<select name="upmg_enable_gallery" id="upmg_enable_gallery" class="chosen-select" style="width:300px">
				<option value="y" <?php selected('y', userpro_media_get_option('upmg_enable_gallery')); ?>><?php _e('Yes','userpro-media'); ?></option>
				<option value="n" <?php selected('n', userpro_media_get_option('upmg_enable_gallery')); ?>><?php _e('No','userpro-media'); ?></option>
			</select>
		</td>
	</tr>

	</table>
	
<h3><?php _e('Photo Gallery Settings','userpro-media'); ?></h3>
<table class="form-table">

	<tr valign="top">
		<th scope="row"><label for="upmg_enable_photo_gallery"><?php _e('Enable Photo Gallery','userpro-media'); ?></label></th>
		<td>
			<select name="upmg_enable_photo_gallery" id="upmg_enable_photo_gallery" class="chosen-select" style="width:300px">
				<option value="y" <?php selected('y', userpro_media_get_option('upmg_enable_photo_gallery')); ?>><?php _e('Yes','userpro-media'); ?></option>
				<option value="n" <?php selected('n', userpro_media_get_option('upmg_enable_photo_gallery')); ?>><?php _e('No','userpro-media'); ?></option>
			</select>
		</td>
	</tr>

	<tr valign="top" id="upmg_max_photo_size">
		<th scope="row"><label for="upmg_max_photo_size"><?php _e('Max Upload Size For Images','userpro-media'); ?></label></th>
		<td>
			<input type="text" name="upmg_max_photo_size" id="upmg_max_photo_size" value="<?php echo userpro_media_get_option('upmg_max_photo_size'); ?>" class="regular-text" />
			<span class="description"><?php _e('Please Enter the Max Upload limit for Images in MB','userpro-media'); ?></span>
		</td>	
	</tr>
	
	<tr valign="top" id="upmg_photo_extension_list">
		<th scope="row"><label for="upmg_photo_extension_list"><?php _e('Allowed Extensions','userpro-media'); ?></label></th>
		<td>
			<input type="text" name="upmg_photo_extension_list" id="upmg_photo_extension_list" value="<?php echo userpro_media_get_option('upmg_photo_extension_list'); ?>" class="regular-text" />
			<span class="description"><?php _e('comma separated list of extensions user can upload from front end. for Ex: jpg,jpeg,png,gif','userpro-media'); ?></span>
		</td>
	</tr>
</table>

<h3><?php _e('Video Gallery Settings','userpro-media'); ?></h3>
<table class="form-table">

	<tr valign="top">
		<th scope="row"><label for="upmg_enable_video_gallery"><?php _e('Enable Video Gallery','userpro-media'); ?></label></th>
		<td>
			<select name="upmg_enable_video_gallery" id="upmg_enable_video_gallery" class="chosen-select" style="width:300px">
				<option value="y" <?php selected('y', userpro_media_get_option('upmg_enable_video_gallery')); ?>><?php _e('Yes','userpro-media'); ?></option>
				<option value="n" <?php selected('n', userpro_media_get_option('upmg_enable_video_gallery')); ?>><?php _e('No','userpro-media'); ?></option>
			</select>
		</td>
	</tr>

	<tr valign="top" id="upmg_max_video_size">
		<th scope="row"><label for="upmg_max_video_size"><?php _e('Max Upload Size For Videos','userpro-media'); ?></label></th>
		<td>
			<input type="text" name="upmg_max_video_size" id="upmg_max_video_size" value="<?php echo userpro_media_get_option('upmg_max_video_size'); ?>" class="regular-text" />
			<span class="description"><?php _e('Please Enter the Max Upload limit for Videos in MB','userpro-media'); ?></span>
		</td>	
	</tr>
	
	<tr valign="top" id="upmg_video_extension_list">
		<th scope="row"><label for="upmg_video_extension_list"><?php _e('Allowed Extensions','userpro-media'); ?></label></th>
		<td>
			<input type="text" name="upmg_video_extension_list" id="upmg_video_extension_list" value="<?php echo userpro_media_get_option('upmg_video_extension_list'); ?>" class="regular-text" />
			<span class="description"><?php _e('comma separated list of extensions user can upload from front end. for Ex: jpg,jpeg,png,gif','userpro-media'); ?></span>
		</td>
	</tr>
</table>

<h3><?php _e('Audio Gallery Settings','userpro-media'); ?></h3>
<table class="form-table">

	<tr valign="top">
		<th scope="row"><label for="upmg_enable_audio_gallery"><?php _e('Enable Audio Gallery','userpro-media'); ?></label></th>
		<td>
			<select name="upmg_enable_audio_gallery" id="upmg_enable_audio_gallery" class="chosen-select" style="width:300px">
				<option value="y" <?php selected('y', userpro_media_get_option('upmg_enable_audio_gallery')); ?>><?php _e('Yes','userpro-media'); ?></option>
				<option value="n" <?php selected('n', userpro_media_get_option('upmg_enable_audio_gallery')); ?>><?php _e('No','userpro-media'); ?></option>
			</select>
		</td>
	</tr>

	<tr valign="top" id="upmg_max_audio_size">
		<th scope="row"><label for="upmg_max_audio_size"><?php _e('Max Upload Size For Audios','userpro-media'); ?></label></th>
		<td>
			<input type="text" name="upmg_max_audio_size" id="upmg_max_audio_size" value="<?php echo userpro_media_get_option('upmg_max_audio_size'); ?>" class="regular-text" />
			<span class="description"><?php _e('Please Enter the Max Upload limit for Audios in MB','userpro-media'); ?></span>
		</td>	
	</tr>
	
	<tr valign="top" id="upmg_audio_extension_list">
		<th scope="row"><label for="upmg_audio_extension_list"><?php _e('Allowed Extensions','userpro-media'); ?></label></th>
		<td>
			<input type="text" name="upmg_audio_extension_list" id="upmg_audio_extension_list" value="<?php echo userpro_media_get_option('upmg_audio_extension_list'); ?>" class="regular-text" />
			<span class="description"><?php _e('comma separated list of extensions user can upload from front end. for Ex: jpg,jpeg,png,gif','userpro-media'); ?></span>
		</td>
	</tr>
</table>
<p class="submit">
	<input type="submit" name="submit" id="submit" class="button button-primary" value="<?php _e('Save Changes','userpro-media'); ?>"  />
	<input type="submit" name="reset-options" id="reset-options" class="button" value="<?php _e('Reset Options','userpro-media'); ?>"  />
</p>
</form>