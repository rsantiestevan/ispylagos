<?php

if( !defined('ABSPATH') ){
	exit;
}

if( !class_exists( 'UPMGHooksActions' ) ){
	
	class UPMGHooksActions{
		
		private static $_instance;
		
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
		
		function __construct(){
			if( userpro_media_get_option('upmg_enable_gallery') == 'y'){
				add_action( 'userpro_after_fields', array( $this, 'media_gallery_tab' ), 11, 1 );
			}	
		}
		
		function media_gallery_tab( $args ){
			if( $args['template'] == 'edit' || $args['template']=='view'){
			?>
			<div class='userpro-section userpro-column userpro-collapsible-1 userpro-collapsed-0 upmg-section'><?php _e('Media Gallery','userpro-media');?></div>
			<?php
			include_once UPMG_PATH.'templates/upmg-tabs.php';
			}
		}
	}
	
	$upmg_hooks_actions = UPMGHooksActions::instance();
}