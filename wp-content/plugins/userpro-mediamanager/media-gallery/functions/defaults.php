<?php

	/* get a global option */
	function userpro_media_gallery_get_option( $option ) {
		$userpro_default_options = userpro_media_default_options();
		$settings = get_option('upmg_options');
		switch($option){
		
			default:
				if (isset($settings[$option])){
					return $settings[$option];
				} else {
					return $userpro_default_options[$option];
				}
				break;
	
		}
	}
	
	/* set a global option */
	function userpro_media_gallery_set_option($option, $newvalue){
		$settings = get_option('upmg_options');
		$settings[$option] = $newvalue;
		update_option('upmg_options', $settings);
	}
	
	/* default options */
	function userpro_media_gallery_default_options(){
		$array = array();
	}	