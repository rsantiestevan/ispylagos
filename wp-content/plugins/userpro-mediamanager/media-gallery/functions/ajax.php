<?php

if( !defined('ABSPATH') ){
	exit;
}

if( !class_exists( 'UPMGAjax' ) ){

	class UPMGAjax{

		private static $_instance;

		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}

		function __construct(){
				
			add_action('wp_ajax_add_new_gallery', array($this,'add_new_gallery') );
			add_action('wp_ajax_nopriv_add_new_gallery', array($this,'add_new_gallery') );
				
			add_action('wp_ajax_upmg_upload_media', array($this,'upmg_upload_media') );
			add_action('wp_ajax_nopriv_upmg_upload_media', array($this,'upmg_upload_media') );
			
			add_action('wp_ajax_upmg_save_gallery', array($this,'upmg_save_gallery') );
			add_action('wp_ajax_nopriv_upmg_save_gallery', array($this,'upmg_save_gallery') );
			
			add_action('wp_ajax_upmg_show_medias', array($this,'upmg_show_medias') );
			add_action('wp_ajax_nopriv_upmg_show_medias', array($this,'upmg_show_medias') );
			
			add_action('wp_ajax_upmg_remove_media', array($this,'upmg_remove_media') );
			add_action('wp_ajax_nopriv_upmg_remove_media', array($this,'upmg_remove_media') );
			
			add_action('wp_ajax_upmg_remove_gallery', array($this,'upmg_remove_gallery') );
			add_action('wp_ajax_nopriv_upmg_remove_gallery', array($this,'upmg_remove_gallery') );
		}

		function add_new_gallery(){
			$mode = $_POST['mode'];
			ob_start();
			include_once UPMG_PATH.'templates/upmg-media-container.php';
			$output = ob_get_contents();
			ob_end_clean();
			echo json_encode(array('output'=>$output));
			die();
		}

		function upmg_upload_media(){
			if( isset($_FILES["userpro_file"]) ) {
				global $userpro;
				$wp_upload_dir = wp_upload_dir();
				$ret = array();
				$file_extension = strtolower(strrchr($_FILES["userpro_file"]["name"], "."));
				$unique_id = uniqid();
				$target_file = $wp_upload_dir['path'] . "/" . $unique_id . $file_extension;
				$media_name = $unique_id . $file_extension;
				move_uploaded_file( $_FILES["userpro_file"]["tmp_name"], $target_file );
				$media_caption = isset($_POST['caption']) && !empty($_POST['caption'])?$_POST['caption']:$_FILES["userpro_file"]["name"];
				$attachment_id = $this->upmg_insert_attachment($target_file,$media_caption);
				if( !empty($attachment_id) ){
					echo $attachment_id;
				}	
			}
			die();
		}

		function upmg_insert_attachment( $filename, $media_caption ){
			$parent_post_id = 0;

			// Check the type of file. We'll use this as the 'post_mime_type'.
			$filetype = wp_check_filetype( basename( $filename ), null );

			// Get the path to the upload directory.
			$wp_upload_dir = wp_upload_dir();

			// Prepare an array of post data for the attachment.
			$attachment = array(
								'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
								'post_mime_type' => $filetype['type'],
								'post_title'     => basename( $filename ),
								'post_excerpt'	 => $media_caption,
								'post_content'   => '',
								'post_status'    => 'inherit'
							);

			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );

			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			return $attach_id;
		}
		
		function upmg_save_gallery(){
			$media_galleries = get_user_meta(get_current_user_id(),'upmg_'.$_POST['upmg_gallery_type'],true);
			$user_id = $_POST['user_id'];
			if( empty( $media_galleries ) ){
				$media_galleries = array();
			}
			$mode = $_POST['mode'];
			$template = $_POST['template'];
			$message = '';
			if( $mode == 'add'){
				$media_galleries[] = array( 'gallery_title'=>$_POST['gallery_title'],'medias'=>$_POST['medias']);
				update_user_meta( $user_id, 'upmg_'.$_POST['upmg_gallery_type'], $media_galleries);
				ob_start();
				$medias = explode(',',$_POST['medias']);
				foreach($medias as $media ){
					if( !empty($media)){
						include UPMG_PATH.'templates/upmg-single-'.$_POST['upmg_gallery_type'].'.php';
					}
				}
				include UPMG_PATH.'templates/upmg-template-add-media.php';
				$media_content = ob_get_contents();
				ob_end_clean();
				$gallery_id = sizeof($media_galleries)-1;
				
				$media_title = "<a href='#' class='upmg-gallery active' data-gallery_id = ".$gallery_id. " >". $_POST['gallery_title']." </a>";
    			$media_title.= "<div class='upmg-edit-title' data-gallery_id = $gallery_id data-upmg_title=".$_POST['gallery_title']."><i class='userpro-icon-upmg-edit'></i></div>";
				$message = 'Gallery created successfully';
				$output = json_encode(array('media_content'=>$media_content,'media_title'=>$media_title,'message'=>$message));
			}else if( $mode == 'edit' ){
				$media_galleries = get_user_meta( get_current_user_id(),'upmg_'.$_POST['upmg_gallery_type'],true );
				$medias = $media_galleries[$_POST['gallery_id']]['medias'];
				$medias.=$_POST['medias'];
				$media_galleries[$_POST['gallery_id']]['medias'] = $medias;
				update_user_meta( $user_id, 'upmg_'.$_POST['upmg_gallery_type'], $media_galleries);
				ob_start();
				$medias = explode(',',$_POST['medias']);
				foreach($medias as $media ){
					if( !empty($media)){
						include UPMG_PATH.'templates/upmg-single-'.$_POST['upmg_gallery_type'].'.php';
					}
				}
				$media_content = ob_get_contents();
				ob_end_clean();
				$message = "Media added successfully";
				$output = json_encode(array('media_content'=>$media_content,'message'=>$message));
			}else if( $mode == 'edit_title' ){
				$gallery_id = $_POST['gallery_id'];
				$media_galleries = get_user_meta( $user_id,'upmg_'.$_POST['upmg_gallery_type'],true );
				$media_galleries[$gallery_id]['gallery_title'] = $_POST['gallery_title'];
				update_user_meta( $user_id, 'upmg_'.$_POST['upmg_gallery_type'], $media_galleries);
				$message = "Gallery title edited successfully";
				$output = json_encode(array('message'=>$message,'content'=>ucfirst($_POST['gallery_title'])));
			}
			
			echo $output;
			die();
		}
		
		function upmg_show_medias($user_id=null,$gallery_id=null,$type=null,$template=null,$ajax=true){
			$user_id = !$ajax?$user_id:$_POST['user_id'];
			$gallery_id = !$ajax?$gallery_id:$_POST['gallery_id'];
			$type = !$ajax?$type:$_POST['type'];
			$template = !$ajax?$template:$_POST['template'];
			
			$galleries = get_user_meta($user_id,'upmg_'.$type,true);
			$gallery_medias = $galleries[$gallery_id];
			ob_start();
			if(isset($gallery_medias['medias'])){
				$medias = explode(',',$gallery_medias['medias']);
				foreach($medias as $media ){
					if( !empty($media)){
						include UPMG_PATH.'templates/upmg-single-'.$type.'.php';
					}
				}
				if( (($user_id == get_current_user_id()) || current_user_can('manage_options')) && $template=='edit' ){
					include UPMG_PATH.'templates/upmg-template-add-media.php';
				}	
			}
			$output = ob_get_contents();
			ob_end_clean();
			if(!$ajax){
				return $output;
			}else{
				echo json_encode(array('output'=>$output));
				die();
			}	
		}
		
		function upmg_remove_media(){
			$user_id = $_POST['user_id'];
			$gallery_id = $_POST['gallery_id'];
			$type = $_POST['type'];
			$media_id = $_POST['media_id'];
			$galleries = get_user_meta($user_id,'upmg_'.$type,true);
			$gallery_medias = $galleries[$gallery_id]['medias'];
			$gallery_medias = str_replace($media_id.",","",$gallery_medias);
						
			$galleries[$gallery_id]['medias'] = $gallery_medias;
			update_user_meta( $user_id, 'upmg_'.$type, $galleries);
			wp_delete_attachment( $media_id );
			echo json_encode(array('output'=>'Media deleted successfully'));
			die();
		}
		
		function upmg_remove_gallery(){
			$user_id = $_POST['user_id'];
			$gallery_id = $_POST['gallery_id'];
			$type = $_POST['type'];
			$galleries = get_user_meta($user_id,'upmg_'.$type,true);
			$gallery_medias = explode(",",$galleries[$gallery_id][medias]);
			foreach( $gallery_medias as $media ){
				if(!empty($media)){
					wp_delete_attachment( $media );
				}
			}
			unset($galleries[$gallery_id]);
			$galleries = array_values($galleries);
			update_user_meta( $user_id, 'upmg_'.$type, $galleries);
		}
		
	}

	$upmg_ajax = UPMGAjax::instance();
}