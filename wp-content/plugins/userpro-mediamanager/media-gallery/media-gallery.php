<?php

if( !defined( 'ABSPATH' ) ){
	exit;
}

if( !class_exists('UPMG') ){
	
	class UPMG{
		
		private static $_instance;
		
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
		
		function __construct(){
			$this->define_constants();
			$this->include_files();
			add_action('wp_enqueue_scripts', array( $this, 'enqueue_scripts_styles' ) );
		}
		
		function define_constants(){
			define('UPMG_URL',userpro_media_url.'/media-gallery/');
			define('UPMG_PATH',userpro_media_path.'/media-gallery/');
		}
		
		function include_files(){
			foreach (glob(UPMG_PATH . 'functions/*.php') as $filename) { require_once $filename; }
		}
		
		function enqueue_scripts_styles(){
			wp_enqueue_style('upmg-style',UPMG_URL.'css/upmg-style.css');
			wp_enqueue_script('upmg-upload-script', UPMG_URL.'scripts/upmg-upload-scripts.min.js','','',true);
			wp_enqueue_script('upmg-scripts', UPMG_URL.'scripts/upmg-scripts.js',array('upmg-upload-script'),'',true);
		}
	}
	
	$upmg = UPMG::instance();
}