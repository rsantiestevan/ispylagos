<div class="upmg-single-media-container">
	<audio width="80" height="80" controls> 
		<source src = "<?php echo wp_get_attachment_url($media);?>" />
	</audio>
	<?php if( (($user_id==get_current_user_id()) || current_user_can('manage_options')) && $template=='edit' ){?>
	<div class="upmg-remove-media" data-media="<?php echo $media;?>">
		<i class='userpro-icon-close upmg-custom-red'></i>
	</div>
	<?php }?>
	<div class="upmg-caption"><?php echo wp_get_attachment_caption($media);?></div>
	
</div>