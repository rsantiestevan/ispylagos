<div class="upmg-single-media-container">
	<?php if( (($user_id==get_current_user_id()) || current_user_can('manage_options')) && $template=='edit' ){?>
	<div class="upmg-remove-media" data-media="<?php echo $media;?>">
		<i class='userpro-icon-close upmg-custom-red'></i>
	</div>
	<?php }?>
	<a class="lightview" href="<?php echo wp_get_attachment_url($media);?>" data-lightview-caption="<?php echo wp_get_attachment_caption($media);?>" data-lightview-group="upmg-group" style="border:1px solid;display:inline-block;">
		<?php echo wp_get_attachment_image($media,array(80,80));?>
	</a>
	
</div>
