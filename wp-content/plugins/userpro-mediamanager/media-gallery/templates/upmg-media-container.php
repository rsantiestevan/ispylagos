<div class="userpro">
	<a href="#" class="userpro-close-popup"><?php _e('Close','userpro'); ?></a>
	<div class="upmg-inner-container">
		<?php if( $mode == 'add' || $mode == 'edit_title'){?>
		<div class="userpro-label">
			<label><?php _e('Enter gallery title','userpro-mediamanager');?></label>
		</div>	
		<div class="userpro-input">
			<input type="text" name="upmg-gallery-title" id="upmg-gallery-title" />
		</div>
		<?php }
			if( $mode == 'add' || $mode == 'edit' ){
		?>
		<div class="upmg-upload-container">
			<div class="upmg-upload-button">
				<?php echo __('Upload','userpro-media');?>
			</div>
			<span class="upmg-upload-info"><?php 
			$extlist = userpro_media_get_option("upmg_".$_POST['type']."_extension_list");
			echo __('( Allowed types : '.$extlist.' )');
			?>
			</span>
		</div>
		<?php }?>
		<div class="userpro-field">
		<input type=button value="Save" id="upmg_save_media" class="userpro-button" data-mode="<?php echo $mode;?>" <?php echo $mode=='edit_title'?'data-gallery_id='.$_POST['gallery_id']:'';?> />
		</div>
	</div>
</div>