<div class="upmg-tabs">
    <ul class="upmg-tab-links">
    	<?php if( userpro_media_get_option('upmg_enable_photo_gallery') == 'y'){?>
        <li class="active"><a href="#upmg-photo"><?php _e('Photos','userpro-mediamanager');?></a></li>
        <?php }
        	  if( userpro_media_get_option('upmg_enable_video_gallery') == 'y'){
        ?>
        <li><a href="#upmg-video"><?php _e('Videos','userpro-mediamanager');?></a></li>
        <?php }
        	 if( userpro_media_get_option('upmg_enable_audio_gallery') == 'y' ){
        ?>
        <li><a href="#upmg-audio"><?php _e('Audio','userpro-mediamanager');?></a></li>
        <?php }?>
    </ul>
</div>

<div class="upmg-tab-content" data-user_id = "<?php echo $args['user_id'];?>" data-template="<?php echo $args['template']; ?>" >
	<?php if( userpro_media_get_option('upmg_enable_photo_gallery') == 'y'){?>
    <div id="upmg-photo" class="upmg-tab active" data-gallery="photo" data-allowed_ext = "<?php echo userpro_media_get_option('upmg_photo_extension_list');?>" data-max_size = "<?php echo userpro_media_get_option('upmg_max_photo_size');?>" >
    	<?php 
			$user_id = $args['user_id'];
			$galleries = array();
			$galleries = get_user_meta( $user_id,'upmg_photo',true );
		?>
    	<div class="upmg-media-galleries">
    	<?php 
    		if( !empty($galleries) ){
    			$i=0;
    			foreach( $galleries as $key=>$val ){
    				$active_class = $i++ == 0 ? 'active' : '';
    				echo "<a href='#' class='upmg-gallery $active_class' data-gallery_id = $key >". $val['gallery_title']." </a>";
    				if( (($args['user_id'] == get_current_user_id()) || current_user_can('manage_options')) && $args['template'] == 'edit' ){
    					echo "<div class='upmg-edit-title' data-gallery_id = $key data-upmg_title=".$val['gallery_title']."><i class='userpro-icon-upmg-edit'></i></div>";
    				}	
    			}
    		}
    	?>
    	</div>
    	<div class="upmg-media-container">
    		<?php 
    			if(empty($galleries)){
    				echo __("No galleries created yet",'userpro-media');
    			}else{
    				$upmg_ajax = UPMGAjax::instance();
    				$params = array('user_id'=>$user_id,'gallery_id'=>0,'type'=>'photo','template'=>$args['template']);
    				echo $upmg_ajax->upmg_show_medias($user_id,'0','photo',$args['template'],false);
    			}
    		?>
    	</div>
    	<?php if( (($args['user_id'] == get_current_user_id()) || current_user_can('manage_options')) && $args['template']=='edit'){?>
        <div class="upmg-add-gallery">
        	<?php echo __('Add New Photo Gallery','userpro-media');?>
        </div>
        
        <div class="upmg-remove-gallery">
        	<?php echo __('Delete Gallery','userpro-media');?>
        </div>
        
        <?php }?>
    </div>
    <?php }?>
 	<?php if( userpro_media_get_option('upmg_enable_video_gallery') == 'y'){?>
    <div id="upmg-video" class="upmg-tab" data-gallery="video" data-allowed_ext = "<?php echo userpro_media_get_option('upmg_video_extension_list');?>" data-max_size = "<?php echo userpro_media_get_option('upmg_max_video_size');?>">
    	<?php 
			$user_id = $args['user_id'];
			$galleries = array();
			$galleries = get_user_meta( $user_id,'upmg_video',true );
		?>
    	<div class="upmg-media-galleries">
    	<?php 
    		if( !empty($galleries) ){
    			$i=0;
    			foreach( $galleries as $key=>$val ){
    				$active_class = $i++ == 0 ? 'active' : '';
    				echo "<a href='#' class='upmg-gallery $active_class' data-gallery_id = $key >". $val['gallery_title']." </a>";
    				if( (($args['user_id'] == get_current_user_id()) || current_user_can('manage_options')) && $args['template'] == 'edit' ){
    					echo "<div class='upmg-edit-title' data-gallery_id = $key data-upmg_title=".$val['gallery_title']."><i class='userpro-icon-upmg-edit'></i></div>";
    				}
    			}
    		}
    	?>
    	</div>
    	<div class="upmg-media-container">
    		<?php 
    			if(empty($galleries)){
    				echo __("No galleries created yet",'userpro-media');
    			}else{
    				$upmg_ajax = UPMGAjax::instance();
    				echo $upmg_ajax->upmg_show_medias($user_id,0,'video',$args['template'],false);
    			}
    		?>
    	</div>
        <?php if( (($args['user_id'] == get_current_user_id()) || current_user_can('manage_options')) && $args['template']=='edit'){?>
        <div class="upmg-add-gallery">
        	<?php echo __('Add New Video Gallery','userpro-media');?>
        </div>
        
        <div class="upmg-remove-gallery">
        	<?php echo __('Delete Gallery','userpro-media');?>
        </div>
        <?php }?>  
    </div>
 	<?php }?>
 	<?php if( userpro_media_get_option('upmg_enable_audio_gallery') == 'y'){?>
    <div id="upmg-audio" class="upmg-tab" data-gallery="audio" data-allowed_ext = "<?php echo userpro_media_get_option('upmg_audio_extension_list');?>" data-max_size = "<?php echo userpro_media_get_option('upmg_max_audio_size');?>" >
    	<?php 
			$user_id = $args['user_id'];
			$galleries = array();
			$galleries = get_user_meta( $user_id,'upmg_audio',true );
		?>
    	<div class="upmg-media-galleries">
    	<?php 
    		if( !empty($galleries) ){
    			$i=0;
    			foreach( $galleries as $key=>$val ){
    				$active_class = $i++ == 0 ? 'active' : '';
    				echo "<a href='#' class='upmg-gallery $active_class' data-gallery_id = $key >". $val['gallery_title']." </a>";
    				if( (($args['user_id'] == get_current_user_id()) || current_user_can('manage_options')) && $args['template'] == 'edit' ){
    					echo "<div class='upmg-edit-title' data-gallery_id = $key data-upmg_title=".$val['gallery_title']."><i class='userpro-icon-upmg-edit'></i></div>";
    				}
    			}
    		}
    	?>
    	</div>
    	<div class="upmg-media-container">
    		<?php 
    			if(empty($galleries)){
    				echo __("No galleries created yet",'userpro-media');
    			}else{
    				$upmg_ajax = UPMGAjax::instance();
    				echo $upmg_ajax->upmg_show_medias($user_id,0,'audio',$args['template'],false);
    			}
    		?>
    	</div>
    	<?php if( (($args['user_id'] == get_current_user_id()) || current_user_can('manage_options')) && $args['template']=='edit'){?>
        <div class="upmg-add-gallery">
        	<?php echo __('Add New Audio Gallery','userpro-media');?>
        </div>
        
        <div class="upmg-remove-gallery">
        	<?php echo __('Delete Gallery','userpro-media');?>
        </div>
        <?php }?>
    </div>
    <?php }?>
</div>