var upmg_media = '';
jQuery(document).ready(function() {
    jQuery(document).on('click', '.upmg-tabs .upmg-tab-links li',function(e)  {
        var currentAttrValue = jQuery(this).find('a').attr('href');
        // Show/Hide Tabs
        jQuery('.upmg-tab-content ' + currentAttrValue).show().siblings().hide();
        jQuery('.upmg-tab').removeClass('active');
        jQuery('.upmg-tab-content ' + currentAttrValue).addClass('active');
 
        // Change/remove current tab to active
        jQuery(this).addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });
    
    jQuery(document).on('click','.upmg-add-gallery',function(){
    	upmg_add_gallery(this);
    });
    
    jQuery(document).on('click','.upmg-remove-gallery',function(){
    	var res = window.confirm("Are you sure you want to delete this gallery?");
    	if(res){
    		upmg_remove_gallery(this);
    	}
    });
    
    jQuery(document).on('click','.upmg-edit-title',function(){
    	upmg_add_gallery(this,'edit_title');
    });
    /*
    jQuery(document).on('click','#upmg_save_media',function(){
    	upmg_save_gallery(this);
    });
    */
    jQuery(document).on('click','.upmg-gallery',function(){
    	upmg_show_media(this);
    });
    
    jQuery(document).on('click','.upmg-remove-media',function(){
    	var res = window.confirm("Are you sure you want to delete this media?");
    	if(res){
    		upmg_remove_media(this);
    	}	
    });
    
    jQuery(document).on('click','.upmg-add-media',function(){
    	upmg_add_media_to_gallery(this);
    });
    /*
    jQuery('.upmg-tab-content .upmg-media-galleries').each(function(){
    	jQuery(this).find('.upmg-gallery').first().trigger('click');
    });
    */
});
/*
jQuery( document ).ajaxComplete(function() {
	 jQuery('.upmg-tab-content .upmg-media-galleries').each(function(){
	    	jQuery(this).find('.upmg-gallery').first().trigger('click');
	 });
});*/

function upmg_add_gallery(elm,mode='add'){
	if (jQuery('body').find('.userpro-overlay').length==0) {
		jQuery('body').append('<div class="userpro-overlay"/><div class="userpro-overlay-inner upmg-overlay-inner"/>');
	}
	
	jQuery.ajax({
		
		url:userpro_ajax_url,
		type:'post',
		data:{'action':'add_new_gallery','mode':mode,'gallery_id':jQuery(elm).data('gallery_id'),'type':jQuery('.upmg-tab-content').find('.upmg-tab.active').data('gallery')},
		dataType:'json',
		success:function(data){
			jQuery(".userpro-overlay-inner").html(data.output);
			userpro_overlay_center('.userpro-overlay-inner');
			if( mode == 'edit_title'){
				var gallery_id = jQuery(elm).data('gallery_id');
				jQuery('#upmg-gallery-title').val(jQuery(elm).parents('.upmg-tab').find('.upmg-media-galleries a[data-gallery_id='+gallery_id+']').html());
				jQuery('#upmg_save_media').show();
			}
			upmg_upload();
		}
	});
}

function upmg_upload(){
	var count=0;
	var upload_limit = 6;
	var filetype = 'photo';
	var obj = jQuery('.upmg-upload-button').upmgUploadFile({
		    url: userpro_ajax_url+"?action=upmg_upload_media&upload_limit="+upload_limit+"&filetype="+filetype,
			multiple:true,
			sequential:false,
			showPreview: false,
			dragDrop: false,
			showAbort: true,
			showDone: true,
			showFileSize: true,
			autoSubmit: false,
			showFileCounter: false,
			allowedTypes:jQuery('.upmg-tab-content').find(".upmg-tab.active").data('allowed_ext'),
			maxFileSize:jQuery('.upmg-tab-content').find(".upmg-tab.active").data('max_size')*1024*1024,
			fileName:'userpro_file',
			customProgressBar: function(obj,s){
				this.statusbar = jQuery("<tr class='ajax-file-upload-statusbar'></tr>");
	            this.preview = jQuery("<img class='upmg-preview' />").width(s.previewWidth).height(s.previewHeight).appendTo(this.statusbar).hide();
	            this.filename = jQuery("<td class='upmg-filename'></td>").appendTo(this.statusbar);
	            this.progressDiv = jQuery("<td class='upmg-progress-div'></td>").appendTo(this.statusbar).hide();
	            this.progressbar = jQuery("<div class='upmg-progress-bar'></div>").appendTo(this.progressDiv);
	            this.cancel = jQuery("<td class='upmg-operations'><i class='userpro-icon-close upmg-custom-red'></i></td>").appendTo(this.statusbar).hide();
	            this.abort = jQuery("<td class='upmg-operations'><i class='userpro-icon-abort upmg-custom-red'></i></td>").appendTo(this.statusbar).hide();
	            this.done = jQuery("<td class='upmg-operations'><i class='userpro-icon-tick upmg-custom-green'></i></td>").appendTo(this.statusbar).hide();
	            this.download = jQuery("<td>" + s.downloadStr + "</td>").appendTo(this.statusbar).hide();
	            this.del = jQuery("<td>" + s.deletelStr + "</td>").appendTo(this.statusbar).hide();
	            
	            if(count++ %2 ==0)
		            this.statusbar.addClass("even");
	            else
	    			this.statusbar.addClass("odd"); 
				return this;
			},
			extraHTML:function(){
				var html = "<div class='userpro-input'><input type='text' name='caption' value='' placeholder='Enter Caption' /></div>";
				return html;
			},
			onSelect:function (files) {
				jQuery('#upmg_save_media').show();
				return true;
			},
			onSuccess:function(files,data,xhr,pd){
				upmg_media = upmg_media + data + ',';
				userpro_overlay_center('.userpro-overlay-inner');
			},
			afterUploadAll:function(obj){
				upmg_save_gallery();
			}
	    });
	jQuery("#upmg_save_media").click(function()
	{
		if( jQuery('#upmg-gallery-title').val()==''){
			alert('Please enter gallery title');
		}else{
			if(jQuery(this).data('mode')=='edit_title'){
				upmg_save_gallery(this);
			}else{
				obj.startUpload();
			}
		}	
	}); 
}

function upmg_save_gallery(elm){
	var mode = jQuery('#upmg_save_media').data('mode');
	var type = jQuery('.upmg-tab-content').find('.upmg-tab.active').data('gallery');
	var user_id = jQuery('.upmg-tab-content').data('user_id');
	var template = jQuery('.upmg-tab-content').data('template');
	var str = "action=upmg_save_gallery&mode="+mode+"&upmg_gallery_type="+type+"&user_id="+user_id+"&template="+template;
	if( mode == 'add' ){
		str+='&gallery_title='+jQuery('#upmg-gallery-title').val()+'&medias='+upmg_media;
	}else if( mode == 'edit' ){
		var gallery_id = jQuery('#upmg-'+type).find('.upmg-media-galleries .upmg-gallery.active').data('gallery_id');
		str+='&medias='+upmg_media+'&gallery_id='+gallery_id;
	}else if( mode == 'edit_title' ){
		var gallery_id = jQuery(elm).data('gallery_id');
		str+='&gallery_title='+jQuery('#upmg-gallery-title').val()+'&gallery_id='+jQuery(elm).data('gallery_id');
	}
	jQuery('#upmg-'+type).find('.upmg-gallery').removeClass('active');
	jQuery.ajax({
		
		url:userpro_ajax_url,
		type:'post',
		data:str,
		dataType:'json',
		success:function(data){
			upmg_media = '';
			if(mode == 'add'){
				jQuery('#upmg-'+type).find('.upmg-media-galleries').append(data.media_title);
				jQuery('#upmg-'+type).find('.upmg-media-container').html(data.media_content);
			}else if(mode == 'edit'){
				jQuery('#upmg-'+type).find('.upmg-add-media').before(data.media_content);
				jQuery('#upmg-'+type).find('.upmg-media-galleries a[data-gallery_id='+gallery_id+']').addClass('active');
			}else if(mode == 'edit_title'){
				jQuery('#upmg-'+type).find('.upmg-media-galleries a[data-gallery_id='+gallery_id+']').html(data.content).addClass('active');
			}
			jQuery('a.userpro-close-popup').trigger('click');
			userpro_overlay_confirmation( data.message );
		}
	});
}

function upmg_show_media(elm){
	
	var gallery_id = jQuery(elm).data('gallery_id');
	var user_id = jQuery(elm).parents('.upmg-tab-content').data('user_id');
	var type = jQuery(elm).parents('.upmg-tab').data('gallery');
	var template = jQuery(elm).parents('.upmg-tab-content').data('template');
	jQuery('#upmg-'+type).find('.upmg-gallery').removeClass('active');
	jQuery(elm).addClass('active');
	jQuery('#upmg-'+type).find('.upmg-media-container').html('<div class="upmg-loader"></div>');
	jQuery.ajax({
		
		url:userpro_ajax_url,
		type:'post',
		data:{'action':'upmg_show_medias','type':type,'gallery_id':gallery_id,'user_id':user_id,'template':template},
		dataType:'json',
		success:function(data){
			jQuery('#upmg-'+type).find('.upmg-media-container').html(data.output);
		}
	});
}

function upmg_remove_media(elm){
	var type = jQuery('.upmg-tab-content').find('.upmg-tab.active').data('gallery');
	var gallery_id = jQuery('#upmg-'+type).find('.upmg-media-galleries .upmg-gallery.active').data('gallery_id');
	var user_id = jQuery(elm).parents('.upmg-tab-content').data('user_id');
	var media_id = jQuery(elm).data('media');
	jQuery.ajax({
		
		url:userpro_ajax_url,
		type:'post',
		data:{'action':'upmg_remove_media','type':type,'gallery_id':gallery_id,'user_id':user_id,'media_id':media_id},
		dataType:'json',
		success:function(data){
			jQuery(elm).parents('.upmg-single-media-container').remove();
		}
	});
}

function upmg_add_media_to_gallery(elm){
	upmg_add_gallery(elm,'edit');
}

function upmg_remove_gallery(elm){
	var type = jQuery('.upmg-tab-content').find('.upmg-tab.active').data('gallery');
	var gallery_id = jQuery('#upmg-'+type).find('.upmg-media-galleries .upmg-gallery.active').data('gallery_id');
	var user_id = jQuery(elm).parents('.upmg-tab-content').data('user_id');
	jQuery.ajax({
		
		url:userpro_ajax_url,
		type:'post',
		data:{'action':'upmg_remove_gallery','type':type,'gallery_id':gallery_id,'user_id':user_id},
		dataType:'json',
		success:function(data){
			jQuery('#upmg-'+type).find('.upmg-media-container').html('');
			jQuery('#upmg-'+type).find('.upmg-media-galleries .upmg-gallery.active').remove();
			jQuery('#upmg-'+type).find('.upmg-media-galleries .upmg-edit-title[data-gallery_id='+gallery_id+']').remove();
			jQuery('#upmg-'+type+' .upmg-media-galleries a').first().trigger('click');
		}
	});
}